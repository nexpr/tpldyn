const p1 = require("./templates/page1.js")

const tpl = require("./helper/template.js")
const p2 = tpl("page2")

console.log("== p1 ==")
console.log(
	p1({
		foo: "p1",
		bar: "<b>bold</b>"
	})
)
console.log("== p2 ==")
console.log(
	p2({
		foo: "p2",
		bar: "<b>bold</b>"
	})
)
