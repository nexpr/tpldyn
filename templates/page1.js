const fs = require("fs")
const html = require("../helper/html.js")
const h1 = require("../helper/h1.js")

const tpl = fs.readFileSync(__dirname + "/page1.html").toString().trim()

module.exports = eval("value => html`" + tpl + "`")
