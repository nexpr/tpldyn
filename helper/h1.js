const html = require("./html.js")

module.exports = (text) => {
	return { html: html`<h1>${text}</h1>` }
}
