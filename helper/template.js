const fs = require("fs")
const cache = {}

const create = (modules, source) => {
	// hide outer variables
	var fs, cache, module, exports, require, __dirname, __filename
	with (modules) { return eval(source) }
}

module.exports = name => {
	if (!cache[name]) {
		const base_path = __dirname + "/../templates"
		const json_path = `${base_path}/${name}.json`
		const def = require(json_path)
		const tpl_path = `${base_path}/${name}.${def.type}`
		const tpl = fs.readFileSync(tpl_path).toString().trim()
		const source = `${def.argument} => ${def.type}\`${tpl}\``
		const modules = {}
		for (const [name, path] of Object.entries(def.requires)) {
			modules[name] = require(`${base_path}/${path}`)
		}
		cache[name] = create(modules, source)
	}
	return cache[name]
}
